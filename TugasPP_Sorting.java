import java.io.IOException;
import java.io.PrintWriter;
import java.util.InputMismatchException;
import java.util.Scanner;

public class TugasPP_Sorting{
	static Scanner input=new Scanner(System.in);
	
	static Object[][] array=new Object[11][5];
	
	static int count=0;
	
	static String choice;
	static String menu;
	static int kode;
	static String nama;
	static String warna;
	static int jumlah;
	static int harga;
	
	  public static void main(String[]args) throws IOException{
		  PrintWriter out = new PrintWriter("Item list.txt");
			PrintWriter sort = new PrintWriter("Sorted list.txt");
		try{
			
		
		
		boolean terminate;
		do{
			System.out.println("1. Input barang");
			System.out.println("2. Item barang");
			
			System.out.print("=> ");
			menu = input.nextLine();
			
			if(menu.contentEquals("0")){
				break;
			}
			
			switch(menu){
			case "1":
				
				 do{
					 
					 terminate=true;
					 
					 do{
						 
							 System.out.print("Kode : ");
						 try{
							 kode=input.nextInt();
							 break;
							 
						 }catch(InputMismatchException e){
							
							 System.out.println("Masukkan angka");
							 System.out.println("Silahkan masukkan ulang");
							 input.nextLine();
							 continue;
							 
						 }
					 }while(true);
					  
					 for(int index=0;index<count;index++){
						 if(kode==(int)array[index][0]){
							 terminate=false;
							 System.out.println("Kode sudah digunakan");
							 System.out.println("Silahkan masukkan ulang\n");
						 }
						 
					 }
					 
				 }while(terminate==false);
				 input.nextLine();
				 do{
					 terminate=true;
					 System.out.print("Nama barang : ");
					 nama=input.nextLine();
					 for(int index=0;index<count;index++){
						 if(nama.contentEquals((String) array[index][1])){
							 terminate=false;
							 System.out.println("Nama sudah digunakan");
							 System.out.println("Silahkan masukkan ulang\n");
						 }
						 
					 }
					 if(nama.matches(".*\\d+.*")){
						 terminate=false;
						 System.out.println("Harus menggunakan huruf");
						 System.out.println("Silahkan masukkan ulang\n");
					 }
					 
				 }while(terminate==false);
				 
				 do{
					 terminate=true;
					 System.out.print("Warna : ");
					 warna=input.nextLine();
					 if(warna.matches(".*\\d+.*")){
						 terminate=false;
						 System.out.println("Harus menggunakan huruf");
						 System.out.println("Silahkan masukkan ulang\n");
					 }
					 
					 
				 }while(terminate==false);
				
				 
					 
					 do{
						 
							 System.out.print("Jumlah : ");
						 try{
							 jumlah=input.nextInt();
							 break;
							 
						 }catch(InputMismatchException e){
							
							 System.out.println("Masukkan angka");
							 System.out.println("Silahkan masukkan ulang");
							 input.nextLine();
							 continue;
							 
						 }
					 }while(true);
					  
					 
				 do{
						 
						 System.out.print("Harga : ");
					 try{
						 harga=input.nextInt();
						 break;
						 
					 }catch(InputMismatchException e){
						
						 System.out.println("Masukkan angka");
						 System.out.println("Silahkan masukkan ulang");
						 input.nextLine();
						 continue;
						 
					 }
				 }while(true);
				 
				
				 
				 
				 
				 
				    array[count][0] = kode;
				    array[count][1] = nama;
				    array[count][2] = warna;
				    array[count][3] = jumlah;
				    array[count][4] = harga;
				    
				    out.println(kode);
					out.println(nama);
					out.println(warna);
					out.println(jumlah);
					out.println(harga);
					out.println("====");
					
					
				    
				   count++;
				input.nextLine();
				break;
				
				
				
			case "2":
				System.out.println("Urut berdasarkan: ");
				System.out.println("1. Kode");
				System.out.println("2. Nama");
				System.out.println("3. Warna");
				System.out.println("4. Jumlah");
				System.out.println("5. Harga");
				System.out.print("=> ");
				choice=input.nextLine();
				
				
				switch(choice){
				case "1":
					
					String temp="";
					int tempInt=-1;
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<count;secondIndex++){
							if( ( (int) array[index][0] ) < ((int)array[secondIndex][0])){
								
								 tempInt = (int) array[secondIndex][0];  
                                 array[secondIndex][0] = array[index][0];  
                                 array[index][0] = tempInt;
                                 
                                 temp = (String) array[secondIndex][1];  
                                 array[secondIndex][1] = array[index][1];  
                                 array[index][1] = temp;
                                 
                                 temp = (String) array[secondIndex][2];  
                                 array[secondIndex][2] = array[index][2];  
                                 array[index][2] = temp;
                                 
                                 tempInt = (int) array[secondIndex][3];  
                                 array[secondIndex][3] = array[index][3];  
                                 array[index][3] = tempInt;
                                 
                                 tempInt = (int) array[secondIndex][4];  
                                 array[secondIndex][4] = array[index][4];  
                                 array[index][4] = tempInt;
							}
						}
					}
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<5;secondIndex++){
							sort.println(array[index][secondIndex]);
						}
						sort.println("====");
					}
					sort.close();
					System.out.println("Sorted list.txt berisi data terurut berhasil dibuat");
					System.out.println("File dapat ditemukan pada folder yang sama dengan program ini");
					break;
				case "2":
					temp="";
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<count;secondIndex++){
							if(((String) array[index][1]).compareToIgnoreCase((String) array[secondIndex][1]) < 0){
								
								tempInt = (int) array[secondIndex][0];  
                                array[secondIndex][0] = array[index][0];  
                                array[index][0] = tempInt;
                                
                                temp = (String) array[secondIndex][1];  
                                array[secondIndex][1] = array[index][1];  
                                array[index][1] = temp;
                                
                                temp = (String) array[secondIndex][2];  
                                array[secondIndex][2] = array[index][2];  
                                array[index][2] = temp;
                                
                                tempInt = (int) array[secondIndex][3];  
                                array[secondIndex][3] = array[index][3];  
                                array[index][3] = tempInt;
                                
                                tempInt = (int) array[secondIndex][4];  
                                array[secondIndex][4] = array[index][4];  
                                array[index][4] = tempInt;
							}
						}
					}
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<5;secondIndex++){
							sort.println(array[index][secondIndex]);
						}
						sort.println("====");
					}
					sort.close();
					System.out.println("Sorted list.txt berisi data terurut berhasil dibuat");
					System.out.println("File dapat ditemukan pada folder yang sama dengan program ini");
					break;
				case "3":
					temp="";
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<count;secondIndex++){
							if(((String) array[index][2]).compareToIgnoreCase((String) array[secondIndex][2]) < 0){
								
								tempInt = (int) array[secondIndex][0];  
                                array[secondIndex][0] = array[index][0];  
                                array[index][0] = tempInt;
                                
                                temp = (String) array[secondIndex][1];  
                                array[secondIndex][1] = array[index][1];  
                                array[index][1] = temp;
                                
                                temp = (String) array[secondIndex][2];  
                                array[secondIndex][2] = array[index][2];  
                                array[index][2] = temp;
                                
                                tempInt = (int) array[secondIndex][3];  
                                array[secondIndex][3] = array[index][3];  
                                array[index][3] = tempInt;
                                
                                tempInt = (int) array[secondIndex][4];  
                                array[secondIndex][4] = array[index][4];  
                                array[index][4] = tempInt;
							}
						}
					}
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<5;secondIndex++){
							sort.println(array[index][secondIndex]);
						}
						sort.println("====");
					}
					sort.close();
					System.out.println("Sorted list.txt berisi data terurut berhasil dibuat");
					System.out.println("File dapat ditemukan pada folder yang sama dengan program ini");
					break;
				case "4":
					temp="";
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<count;secondIndex++){
							if( ( (int) array[index][3] ) < ((int)array[secondIndex][3])){
								
								tempInt = (int) array[secondIndex][0];  
                                array[secondIndex][0] = array[index][0];  
                                array[index][0] = tempInt;
                                
                                temp = (String) array[secondIndex][1];  
                                array[secondIndex][1] = array[index][1];  
                                array[index][1] = temp;
                                
                                temp = (String) array[secondIndex][2];  
                                array[secondIndex][2] = array[index][2];  
                                array[index][2] = temp;
                                
                                tempInt = (int) array[secondIndex][3];  
                                array[secondIndex][3] = array[index][3];  
                                array[index][3] = tempInt;
                                
                                tempInt = (int) array[secondIndex][4];  
                                array[secondIndex][4] = array[index][4];  
                                array[index][4] = tempInt;
							}
						}
						
					}
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<5;secondIndex++){
							sort.println(array[index][secondIndex]);
						}sort.println("====");
					}
					sort.close();
					System.out.println("Sorted file.txt berisi data terurut berhasil dibuat");
					System.out.println("File dapat ditemukan pada folder yang sama dengan program ini");
					break;
				case "5":
					temp="";
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<count;secondIndex++){
							if( ( (int) array[index][4] ) < ((int)array[secondIndex][4])){
								
								tempInt = (int) array[secondIndex][0];  
                                array[secondIndex][0] = array[index][0];  
                                array[index][0] = tempInt;
                                
                                temp = (String) array[secondIndex][1];  
                                array[secondIndex][1] = array[index][1];  
                                array[index][1] = temp;
                                
                                temp = (String) array[secondIndex][2];  
                                array[secondIndex][2] = array[index][2];  
                                array[index][2] = temp;
                                
                                tempInt = (int) array[secondIndex][3];  
                                array[secondIndex][3] = array[index][3];  
                                array[index][3] = tempInt;
                                
                                tempInt = (int) array[secondIndex][4];  
                                array[secondIndex][4] = array[index][4];  
                                array[index][4] = tempInt;
							}
						}
						
					}
					for(int index=0;index<count;index++){
						for(int secondIndex=0;secondIndex<5;secondIndex++){
							sort.println(array[index][secondIndex]);
						}sort.println("====");
					}
					sort.close();
					System.out.println("Sorted list.txt berisi data terurut berhasil dibuat");
					System.out.println("File dapat ditemukan pada folder yang sama dengan program ini");
					break;
				default:
					
					break;
						
				}
				
				out.close();
				System.out.println("Item list.txt berisi daftar barang berhasil dibuat");
				System.out.println("File dapat ditemukan pada folder yang sama dengan program ini");
				break;
			
			default:
				System.out.println("Masukkan angka 1 atau 2 ");
				System.out.println("Silahkan masukkan ulang\n");
				break;
			}
					
			
		}while(menu.contentEquals("2")==false);		
	}finally{
		out.close();
		sort.close();
	}
	  }
	 
}
